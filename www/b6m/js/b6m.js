/*

 b6m

 Copyright (C) 2008-2013 Simó Albert i Beltran

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

if(!$.isArray(b6m))
{
	var b6m = new Array();
}

// Default values can be override with existing b6m array or using URL.
// b6m.fooBar=value is overrided by http://host/b6m?b6mFooBar=value
if(!b6m.zoom) b6m.zoom = 13;
if(!b6m.reloadTime) b6m.reloadTime = 2000;
if(!b6m.pauseTimeout) b6m.pauseTimeout = 0;
if(!b6m.thresholdRemove) b6m.thresholdRemove = 20;
if(!b6m.opacityFactor) b6m.opacityFactor = b6m.thresholdRemove/2;
if(!b6m.splashHTML) b6m.splashHTML = '<h1>Welcome to b6m</h1><h2>Please wait...</h2><p>Meanwhile, you can think about making a donation ;-)</p><p><a href="http://qmp.cat/projects/b6m">b6m project</a></p>';

//Variable for setB6mURL function.
if(!b6m.scriptLocation) b6m.scriptLocation = 'js/b6m.js';


b6m.node = new Array();
b6m.queue = new Array();
b6m.queue.items = new Array();

$(document).ready(setB6mURL);

/**
 * Set base URL of b6m.
 */
function setB6mURL()
{
	if(!b6m.url)
	{
		var filter = new RegExp('.*'+b6m.scriptLocation);

		// static include
		var scripts = document.getElementsByTagName('script');

		for(var script in scripts)
		{
			if(filter.test(scripts[script].src))
			{
				b6m.url = scripts[script].src.replace(b6m.scriptLocation,'');
				prepareB6mEnviroment();
				return;
			}
		}
		// jQuery ajax $.getScript
		if($ != undefined)
		{
			$(document).ajaxSuccess(function(e, xhr, s){
				if(filter.test(s.url))
				{
					filter = new RegExp(b6m.scriptLocation+'.*');
					b6m.url = s.url.replace(filter,'');
					prepareB6mEnviroment();
				}
        	});
		}
	}
}

/**
 * Generate the b6m files URL.
 *
 * @return {String} The b6m files URL.
 */

function getB6mURL()
{
	if(b6m.url)
	{
		return b6m.url;
	}
}

/**
 * Create b6m enviroment if it don't exist.
 */
function prepareB6mEnviroment()
{

	var url = getB6mURL();

	if($('#b6m').length == 0)
	{
		$('<div/>', { 'id': 'b6m' }).appendTo('body');
	}

	getB6mCSS();
}

/**
 * Get the b6m CSS.
 */
function getB6mCSS()
{
	var url = getB6mURL();

	$.ajax({
		url: url+'css/b6m.css',
		success: function(data)
		{
			$('<style></style>').appendTo('head').html(data);
			createB6mSplash();
		}
	});
}

/**
 * Create b6m splash.
 */
function createB6mSplash()
{
	var url = getB6mURL();

	if(b6m.splashHTML)
	{
		$('<div/>', { 'id': 'b6mSplash' }).appendTo('#b6m');
		$(b6m.splashHTML).appendTo('#b6m > #b6mSplash');
	}

	$.getScript(url+'js/jquery.loadImages.1.0.1.min.js', preloadImages);
}

/**
 * Preload images.
 */
function preloadImages()
{

	var url = getB6mURL();

	var images =
	[
		url+'img/ispyisail_Wireless_WiFi_symbol_2.png',
		url+'img/pause.png',
		url+'img/play.png',
		url+'js/img/blank.gif',
		url+'js/img/cloud-popup-relative.png',
		url+'js/img/drag-rectangle-off.png',
		url+'js/img/drag-rectangle-on.png',
		url+'js/img/east-mini.png',
		url+'js/img/layer-switcher-maximize.png',
		url+'js/img/layer-switcher-minimize.png',
		url+'js/img/marker-blue.png',
		url+'js/img/marker-gold.png',
		url+'js/img/marker-green.png',
		url+'js/img/marker.png',
		url+'js/img/measuring-stick-off.png',
		url+'js/img/measuring-stick-on.png',
		url+'js/img/north-mini.png',
		url+'js/img/panning-hand-off.png',
		url+'js/img/panning-hand-on.png',
		url+'js/img/slider.png',
		url+'js/img/south-mini.png',
		url+'js/img/west-mini.png',
		url+'js/img/zoombar.png',
		url+'js/img/zoom-minus-mini.png',
		url+'js/img/zoom-plus-mini.png',
		url+'js/img/zoom-world-mini.png'
	];
	$.loadImages(images, getJqUrlPlugin);
}

/**
 * Get jQuery URL plugin.
 */
function getJqUrlPlugin()
{
	var url = getB6mURL();

	$.getScript(url+'js/jquery.url.js', processParamsAndGetOpenLayers);
}

/**
 * Read URL paremeters and get OpenLayers javascript.
 */
function processParamsAndGetOpenLayers()
{
	var param;
	param = $.url.param('b6mPauseTimeout');
	if(isFinite(param)) b6m.pauseTimeout = param * 1000;

	param = $.url.param('b6mReloadTime');
	if(isFinite(param)) b6m.reloadTime = param * 1000;

	param = $.url.param('b6mType');
	if(param) b6m.type = param;

	param = $.url.param('b6mZoom');
	if(param) b6m.zoom = param;

	param = $.url.param('b6mLat');
	if(param) b6m.latitude = param;

	param = $.url.param('b6mLon');
	if(param) b6m.longitude = param;

	param = $.url.param('b6mOpacityFactor');
	if(isFinite(param)) b6m.opacityFactor = param;

	param = $.url.param('b6mThresholdRemove');
	if(param) b6m.thresholdRemove = param;

	param = $.url.param('b6mConfigure');
	if(param)
	{
		b6m.configure = param;
		b6m.type = 'pointer';
		configureEnviroment(b6m.configure);
	}


	param = $.url.param('b6mDecentralized');
	if(param) b6m.decentralized = param;

	$.getScript(getB6mURL()+'js/OpenLayers.js', makeMap);
}

/**
 * Show a node configuration form.
 */
function configureEnviroment(id)
{
	$('#b6mConfigure').remove();

	$(
		'<div/>',
		{ 'id': 'b6mConfigure'}
	).css
	(
		{
			'position': 'absolute',
			'z-index': '1000',
			'margin': '1em 5em',
			'padding': '0.5em 1em',
			'background-color': 'white',
			'border-radius': '1em',
			'moz-border-radius': '1em',
			'-webkit-border-radius': '1em',
			'box-shadow': 'inset 0 0 5px 5px #eee',
			'-moz-box-shadow': 'inset 0 0 5px 5px #eee',
			'-webkit-box-shadow': 'inset 0 0 5px 5px #eee',
		}
	).prependTo('#b6m');

	$(
		'<div/>',
		{
			'id': 'b6mConfigureClose',
			'class': 'olPopupCloseBox'
		}
	).css(
		{
			'width': '17px',
			'height': '17px',
			'position': 'absolute',
			'right': '13px',
			'top': '10px',
			'z-index': '1000'
		}
	).appendTo('#b6mConfigure');

	var html = '<form><fieldset>';
	html = html + '<legend>Node configuration</legend>';
	html = html + '<label for="id">Node ID:</label>';
	html = html + '<select id="b6mConfigureNodeId" name="id">';
	for(globalId in b6m.node)
	{
		if(!b6m.node[globalId].fail)
		{
			var selected = '';
			if(id == globalId)
			{
				selected = ' selected="selected"';
			}
			html = html + '<option' + selected + '>' + globalId + '</option>';
		}
	}
	html = html + '</select><br/>';
	html = html + 'Click on the map to set a latitude and a longitude.<br/>';
	html = html + '<label for="b6mLatitude">Latitude:</label>';
	html = html + '<input type="text" id="b6mLatitude" value="' + b6m.node[id].data.b6mGeoposition.latitude + '"/><br/>';
	html = html + '<label for="b6mLongitude">Longitude:</label>';
	html = html + '<input type="text" id="b6mLongitude" value="' + b6m.node[id].data.b6mGeoposition.longitude + '"/><br/>';
	html = html + '<input id="b6mConfigureSubmit" type="submit"/>';
	html = html + '</fieldlabel></form>';
	html = html + '<div id="b6mConfigureNodeResult"/>';

	$(html).appendTo('#b6mConfigure');

	$('#b6mConfigureNodeId').change(
		function()
		{
			var globalId = $('select#b6mConfigureNodeId option:selected').val();
			$('#b6mLatitude').val(b6m.node[globalId].data.b6mGeoposition.latitude);
			$('#b6mLongitude').val(b6m.node[globalId].data.b6mGeoposition.longitude);
         }
    );

	$('#b6mConfigureSubmit').click(
		function()
		{
			var query = '&configure=' + $('select#b6mConfigureNodeId option:selected').val() + '&latitude=' + $('#b6mLatitude').val() + '&longitude=' + $('#b6mLongitude').val();
			b6m.configure_ajax = $.getJSON(
				'/cgi-bin/b6m-jsonp?callback=?',
				query,
				function(data)
				{
					var html ='';
					if(data.saved)
					{
						html = html + '<strong>saved:</strong><br/>';
						html = html + 'node: ' + data.saved.globalId + '<br/>';
						html = html + 'longitude: ' + data.saved.longitude + '<br/>';
						html = html + 'latitude: ' + data.saved.longitude + '<br/>';
						getData(id,'b6mGeoposition');
					}
					else
					{
						html = html + '<strong>not saved</strong>';
					}
					$('#b6mConfigureNodeResult').html(html);
				}
			).error(
				function()
				{
					$('<div><strong>not saved</strong></div>').appendTo('#b6mConfigure');
				}
			);
			return false;
		}
	);

	$('#b6mConfigureClose').click(
		function()
		{
			$('#b6mConfigure').remove();
			b6m.map.getLayer('pointer').setVisibility(false);
			return false;
		}
	);
}

/**
 * Create the map.
 */
function makeMap()
{
	OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;

	OpenLayers._getScriptLocation = function () {
		return getB6mURL() + 'js/';
	};

	var options = {
		projection: new OpenLayers.Projection("EPSG:900913"),
		displayProjection: new OpenLayers.Projection("EPSG:4326"),
//		projection: new OpenLayers.Projection("EPSG:4326"),
		units: "m",
		numZoomLevels: 18,
		fractionalZoom: true,
		maxResolution: 156543.0339,
		maxExtent:
			new OpenLayers.Bounds(
				-20037508, -20037508, 20037508, 20037508.34
			),
		controls:
		[
			new OpenLayers.Control.Navigation(),
			new OpenLayers.Control.PanZoomBar(),
			new OpenLayers.Control.ScaleLine(),
			new OpenLayers.Control.MousePosition(),
			new OpenLayers.Control.LayerSwitcher(),
			new OpenLayers.Control.OverviewMap({maximized: true})
		]
	};

	b6m.map = new OpenLayers.Map('b6m', options);

	// Client Zoom: http://openlayers.org/dev/examples/clientzoom.html
	var osmLayer = new OpenLayers.Layer.OSM(null, null, {
		resolutions: [156543.03390625, 78271.516953125, 39135.7584765625,
			19567.87923828125, 9783.939619140625, 4891.9698095703125,
			2445.9849047851562, 1222.9924523925781, 611.4962261962891,
			305.74811309814453, 152.87405654907226, 76.43702827453613,
			38.218514137268066, 19.109257068634033, 9.554628534317017,
			4.777314267158508, 2.388657133579254, 1.194328566789627,
			0.5971642833948135, 0.25, 0.1, 0.05],
		serverResolutions: [156543.03390625, 78271.516953125, 39135.7584765625,
			19567.87923828125, 9783.939619140625,
			4891.9698095703125, 2445.9849047851562,
			1222.9924523925781, 611.4962261962891,
			305.74811309814453, 152.87405654907226,
			76.43702827453613, 38.218514137268066,
 			19.109257068634033, 9.554628534317017,
			4.777314267158508, 2.388657133579254,
			1.194328566789627, 0.5971642833948135],
		transitionEffect: 'resize'
	});
	b6m.map.addLayer(osmLayer);

	var linkStyle = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style.default);
	linkStyle.fillOpacity = 0.2;
	linkStyle.graphicOpacity = 1;

	var goodLinkStyle = OpenLayers.Util.extend({}, linkStyle);
	goodLinkStyle.strokeColor = 'green';
	goodLinkStyle.fillColor = 'green';
	goodLinkStyle.strokeWidth = 3;

	var goodLinksLayer = new OpenLayers.Layer.Vector('Good Links', {style: goodLinkStyle});
	goodLinksLayer.id = 'goodLinks';
	b6m.map.addLayer(goodLinksLayer);

	var normalLinkStyle = OpenLayers.Util.extend({}, linkStyle);
	normalLinkStyle.strokeColor = 'orange';
	normalLinkStyle.fillColor = 'orange';
	normalLinkStyle.strokeWidth = 2;

	var normalLinksLayer = new OpenLayers.Layer.Vector('Normal Links', {style: normalLinkStyle});
	normalLinksLayer.id = 'normalLinks';
	b6m.map.addLayer(normalLinksLayer);

	var badLinkStyle = OpenLayers.Util.extend({}, linkStyle);
	badLinkStyle.strokeColor = 'red';
	badLinkStyle.fillColor = 'red';

	var badLinksLayer = new OpenLayers.Layer.Vector('Bad Links', {style: badLinkStyle});
	badLinksLayer.id = 'badLinks';
	b6m.map.addLayer(badLinksLayer);

	var nodesSearchStyle = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style.default);
	nodesSearchStyle.strokeColor = 'violet';
	nodesSearchStyle.fillColor = 'violet';

	var nodesSearchLayer = new OpenLayers.Layer.Vector('Nodes searching Internet', {'style': nodesSearchStyle});
	nodesSearchLayer.id = 'nodesSearch';
	b6m.map.addLayer(nodesSearchLayer);

	var nodesInternetStyle = OpenLayers.Util.extend({}, OpenLayers.Feature.Vector.style.default);
	nodesInternetStyle.strokeColor = 'blue';
	nodesInternetStyle.fillColor = 'blue';

	var nodesInternetLayer = new OpenLayers.Layer.Vector('Nodes offering Internet', {'style': nodesInternetStyle});
	nodesInternetLayer.id = 'nodesInternet';
	b6m.map.addLayer(nodesInternetLayer);

	selectControl = new OpenLayers.Control.SelectFeature(
		[
			nodesSearchLayer,
			nodesInternetLayer
		],
		{
			onSelect: onFeatureSelect,
			onUnselect: onFeatureUnselect
		}
	);
	b6m.map.addControl(selectControl);
	selectControl.activate();

	var nodesLayer = new OpenLayers.Layer.Markers( 'Nodes' );
	nodesLayer.id = 'nodes';
	b6m.map.addLayer(nodesLayer);

	// Layer of the pointer.
	var pointerLayer = new OpenLayers.Layer.Vector('Pointer');
	pointerLayer.id = 'pointer';

	// Hide the layer of the nodes if map type is pointer,
	// and hide the layer of the pointer if map type isn't pointer.
	if(b6m.type == 'pointer')
	{
		nodesLayer.visibility = false;
	}
	else
	{
		pointerLayer.visibility = false;
	}

	b6m.map.addLayer(pointerLayer);

	// Create image of pointer and get latitude and longitude if layer of the pointer is clicked.
	var map = b6m.map;
	map.events.register('click', map, handleMapClick);
	function handleMapClick(e)
	{
		var lonlat = b6m.map.getLonLatFromViewPortPx(e.xy);
		var box = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(lonlat.lon,lonlat.lat));
		pointerLayer.destroyFeatures();
		pointerLayer.addFeatures(box);
		lonlat.transform(b6m.map.getProjectionObject(), b6m.map.displayProjection);
		if($('#b6mLatitude')) $('#b6mLatitude').val(lonlat.lat);
		if($('#b6mLongitude')) $('#b6mLongitude').val(lonlat.lon);
	}

	var sources = $.url.param('node');
	if(sources)
	{
		var source = sources.split(',');
		for(var i in source)
		{
			getData(source[i]);
		}
	}

	runQueue();

	b6m.clearOriginatorTimeout = setInterval('clearOriginators();', b6m.reloadTime * b6m.thresholdRemove);

	if(b6m.latitude && b6m.longitude)
	{
		centerMap(b6m.longitude, b6m.latitude);
	}
	addPausePanel();
	if(b6m.pauseTimeout != 0) b6m.pauseTimeoutId = setTimeout('setPause();', b6m.pauseTimeout);

}

/**
 * show popup with node information when select node.
 */
function onFeatureSelect(feature)
{
	var popupId = 'popup_' + feature.id.replace(/\./g,'_');
	html = getInfoNode(feature.id).innerHTML + '<a id="' + popupId + '_configure" href="">configure</a>';
	b6m.node[feature.id].popup = new OpenLayers.Popup.FramedCloud
	(
		popupId,
		feature.geometry.getBounds().getCenterLonLat(),
		null,
		html,
		null,
		true,
		onPopupClose
	);
	b6m.node[feature.id].popup.feature = feature;
	b6m.map.addPopup(b6m.node[feature.id].popup);
	$('#' + popupId + '_configure').click
	(
		function()
		{
			configureEnviroment(feature.id);
			b6m.map.getLayer('pointer').setVisibility(true);
			b6m.node[feature.id].popup.feature = null;
			b6m.node[feature.id].popup.destroy();
			b6m.node[feature.id].popup = null;
			return false;
		}
	);
}

/**
 * Erase popup when unselect node.
 */
function onFeatureUnselect(feature)
{
	if (b6m.node[feature.id].popup)
	{
		b6m.map.removePopup(b6m.node[feature.id].popup);
		b6m.node[feature.id].popup.feature = null;
		b6m.node[feature.id].popup.destroy();
		b6m.node[feature.id].popup = null;
	}
}

/**
 * Unselect node when close popup or erase if node don't exist.
 */
function onPopupClose(event)
{
	if(this.feature.layer)
	{
		selectControl.unselect(this.feature);
		//selectControl.unselect(b6m.node[this.feature.id].popup.feature);
	}
	else
	{
			var id = this.feature.id;
			b6m.node[id].popup.feature = null;
			b6m.node[id].popup.destroy();
			b6m.node[id].popup = null;
	}
}

/**
 * Request information.
 */
function getData(id, action)
{
	if(!id)	return;

	if(!b6m.queue.items[id])
	{
		b6m.queue.items[id] = new Array();
	}
	if(action)
	{
		b6m.queue.items[id][action] = true;

		if(action != 'originators')
		{
			b6m.queue.items[id].originators = true;
		}
	}
	else
	{
		b6m.queue.items[id].originators = true;
		b6m.queue.items[id].descriptions = true;
		b6m.queue.items[id].b6mGeoposition = true;
	}
}

/**
 * Relaod information.
 */
function runQueue()
{
	var args='&get=';
	for(var id in b6m.queue.items)
	{
		args = args + '+' + id + ':';
		for(var arg in b6m.queue.items[id])
		{
			args = args + ',' + arg;
		}
	}
	var url='/cgi-bin/b6m-jsonp?callback=?';
	b6m.queue.ajax = $.ajax({
		url: url,
		timeout: b6m.reloadTime * 10,
		type: 'POST',
		dataType: 'jsonp',
		data: args
	}).success(
		function(data)
		{
			if(!b6m.pause) processData(data,id);
		}
	).error(
		function()
		{
			if(!b6m.pause)
			{
				clearTimeout(b6m.queue.timeout);
				b6m.queue.timeout = setTimeout('runQueue();', b6m.reloadTime);
			}
		}
	);
}

/**
 * Process de reload information.
 */
function processData(data,id)
{
	delete(b6m.queue.items[id]);
	for(var id in data)
	{
		delete(b6m.queue.items[id]);
		createNodeDataBase(id);
		for(var item in data[id])
		{
			b6m.node[id].newData[item] = data[id][item];
		}
		update(id);
	}

	getAllData();
	clearTimeout(b6m.queue.timeout);
	b6m.queue.timeout = setTimeout('runQueue();', b6m.reloadTime);
}

/**
 * Get all data.
 */
function getAllData()
{
	for(var id in b6m.node)
	{
		if(!b6m.node[id].fail)
		{
			getData(id, 'originators');
			getData(id, 'b6mLinks');
			if(
				!b6m.node[id].data
				||
				!b6m.node[id].data.b6mGeoposition
				||
				!b6m.node[id].data.b6mGeoposition.mode
				||
				b6m.node[id].data.b6mGeoposition.mode != 'static'
			)
			{
				getData(id, 'b6mGeoposition');
			}
		}
	}
}

/**
 * Clear the nodes without information recent.
 */
function clearOriginators()
{
	for(var id in b6m.node)
	{
		var d = new Date();
		if(b6m.node[id].lastOriginator + b6m.thresholdRemove * 1000 < d.getTime())
		{
			b6m.node[id].fail = true;
			eraseNode(id);
		}
	}
}

/**
 * Center the map.
 */
function centerMap(lon,lat)
{
		b6m.map.setCenter(new OpenLayers.LonLat(lon, lat).transform(
			new OpenLayers.Projection('EPSG:4326'),
			new OpenLayers.Projection('EPSG:900913')
		), b6m.zoom);
}

/**
 * Relpace play button for pause button.
 */
function addPausePanel()
{
	var control_pause = new OpenLayers.Control.Button({
		displayClass: 'pauseButton',
		trigger: setPause,
		trigger: function(){setPause();},
		title: 'Pause'
	});

	b6m.panel = new OpenLayers.Control.Panel();
	b6m.panel.addControls(control_pause);
	b6m.map.addControl(b6m.panel);
}

/**
 * Stop the information reload.
 */
function setPause()
{
	b6m.pause = true;

	clearTimeout(b6m.pauseTimeoutId);

	b6m.map.removeControl(b6m.panel);

	clearTimeout(b6m.queue.timeout);
	b6m.queue.ajax.abort();

	clearInterval(b6m.clearOriginatorTimeout);

	addPlayPanel();
}

/**
 * Relpace pause button for play button.
 */
function addPlayPanel()
{
	var control_play = new OpenLayers.Control.Button({
		displayClass: 'playButton',
		trigger: setPlay,
		title: 'Play'
	});

	b6m.panel = new OpenLayers.Control.Panel();
	b6m.panel.addControls(control_play);
	b6m.map.addControl(b6m.panel);
}

/**
 * Continue the information reload.
 */
function setPlay()
{
	b6m.pause = false;

	b6m.map.removeControl(b6m.panel);

	b6m.clearOriginatorTimeout = setInterval('clearOriginators();', b6m.reloadTime * b6m.thresholdRemove);

	runQueue();
	addPausePanel();
}

/**
 * Draw the links of a node.
 */
function drawLinks(id)
{
	// Draw links from this node to other nodes.
	for(var link in b6m.node[id].data.b6mLinks)
	{
		drawLink(id, link)
	}


	// Draw links from other nodes to this node.
	for(var orig in b6m.node)
	{
		if(b6m.node[orig].data.b6mLinks)
		{
			for(var link in b6m.node[orig].data.b6mLinks)
			{
				if(b6m.node[orig].data.b6mLinks[link].globalId == id)
				{
					drawLink(orig, link);
				}
			}
		}
	}
}

/**
 * Draw a link of a node.
 */
function drawLink(orig, link)
{
	// Skip this link if is marked has drawn and destination is up.
	var dest = b6m.node[orig].data.b6mLinks[link].globalId;
	if(
		b6m.node[orig].data.b6mLinks[link].vector
		&&
		!b6m.node[dest].fail
	)return;

	eraseLink(orig,link);

	if(!existsNode(orig)) return;
	if(!existsNode(dest)) return;

	if(!hasGeoposition(orig)) return;
	if(!hasGeoposition(dest)) return;

	var layer = chooseLinkLayer(b6m.node[orig].data.b6mLinks[link].quality);

	drawLine(
		orig,
		link,
		b6m.node[orig].data.b6mGeoposition.longitude,
		b6m.node[orig].data.b6mGeoposition.latitude,
		b6m.node[dest].data.b6mGeoposition.longitude,
		b6m.node[dest].data.b6mGeoposition.latitude,
		layer
	);
}

/**
 * The node has geoposition.
 *
 * @return bolean
 */
function hasGeoposition(id)
{
	if(
		b6m.node[id]
		&&
		b6m.node[id].data
		&&
		b6m.node[id].data.b6mGeoposition
		&&
		b6m.node[id].data.b6mGeoposition.longitude
		&&
		b6m.node[id].data.b6mGeoposition.latitude
	)
	{
		if(b6m.node[id].data.b6mGeoposition.mode != "fake")
		{
			getData(id,'b6mGeoposition');
		}
		return true;
	}
	else
	{
		getData(id,'b6mGeoposition');
		return false;
	}
}

/**
 * Choose a OpenLayers.Layer for link with a specific quality.
 *
 * @return OpenLayers.Layer
 */
function chooseLinkLayer(quality)
{
	var layer;

	if (quality == 'good')
	{
		layer = b6m.map.getLayer('goodLinks');
	}
	else if (quality == 'bad')
	{
		layer = b6m.map.getLayer('badLinks');
	}
	else
	{
		//if (quality == 'normal')
		layer = b6m.map.getLayer('normalLinks');
	}

	return layer;
}

/**
 * Update information.
 */
function update(id)
{
	for (var item in b6m.node[id].newData)
	{
		switch(item)
		{
			case 'b6mGeoposition':
				updateB6mGeoposition(id,updateB6mLinks);
			break;
			case 'b6mLinks':
				updateB6mLinks(id);
			break;
			case 'originators':
				updateOriginators(id);
			break;
			default:
				b6m.node[id].data[item] = b6m.node[id].newData[item];
				delete(b6m.node[id].newData[item]);
			break;
		}
	}
}

/**
 * Update geoposition information.
 */
function updateB6mGeoposition(id,callback)
{
	if(!b6m.node[id].newData.b6mGeoposition)
	{
		getData(id,'b6mGeoposition');
		return;
	}

	if
	(
		(
			!b6m.node[id].data.b6mGeoposition

			||

			b6m.node[id].data.b6mGeoposition.latitude
			!=
			b6m.node[id].newData.b6mGeoposition.latitude

			||

			b6m.node[id].data.b6mGeoposition.longitude
			!=
			b6m.node[id].newData.b6mGeoposition.longitude
		)

		&&

		isFinite(b6m.node[id].newData.b6mGeoposition.longitude)
		&&
		isFinite(b6m.node[id].newData.b6mGeoposition.latitude)
	)
	{
		b6m.node[id].data.b6mGeoposition = b6m.node[id].newData.b6mGeoposition;
		delete(b6m.node[id].newData.b6mGeoposition);
		// Redraw node.
		updateNode(id);
		// If positon of node was changed, links may be was changed.
		callback(id);
	}
	else
	{
		delete(b6m.node[id].newData.b6mGeoposition);
	}
}

/**
 * Update links information.
 */
function updateB6mLinks(id)
{
	if(!b6m.node[id].newData.b6mLinks)
	{
		getData(id,'b6mLinks');
		return;
	}

	if(b6m.node[id].marker)
	{
		for (var oldLink in b6m.node[id].data.b6mLinks)
		{
			var erase = true;
			for (var newLink in b6m.node[id].newData.b6mLinks)
			{
				if(
					b6m.node[id].data.b6mLinks[oldLink].globalId
					==
					b6m.node[id].newData.b6mLinks[newLink].globalId
					&&
					b6m.node[id].data.b6mLinks[oldLink].viaDev
					==
					b6m.node[id].newData.b6mLinks[newLink].viaDev
				)
				{
					erase = false;
					updateLink(id,oldLink,newLink);
				}
			}
			if(erase)
			{
				eraseLink(id,oldLink);
			}
		}
	}
	b6m.node[id].data.b6mLinks = b6m.node[id].newData.b6mLinks;
	delete(b6m.node[id].newData.b6mLinks);

	if
	(
		!b6m.node[id].marker
		||
		!b6m.node[id].vector
	)
	{
		updateNode(id);
	}

	drawLinks(id);
}

/**
 * Update originators information.
 */
function updateOriginators(id)
{
	for(var originator in b6m.node[id].newData.originators)
	{
		var globalId = b6m.node[id].newData.originators[originator].globalId;

		createNodeDataBase(globalId);

		b6m.node[globalId].data.originator = b6m.node[id].newData.originators[originator];

		var d = new Date();
		b6m.node[globalId].lastOriginator = d.getTime();

		if(b6m.node[globalId].data.originator.lastRef > b6m.thresholdRemove)
		{
			b6m.node[globalId].fail = true;
			eraseNode(globalId);
		}
		else
		{
			b6m.node[globalId].fail = false;

			if
			(
				!b6m.node[globalId].marker
				||
				!b6m.node[globalId].vector
			)
			{
				updateNode(globalId);
			}

			if(b6m.node[globalId].marker)
			{
				b6m.node[globalId].marker.setOpacity(Math.exp(-b6m.node[globalId].data.originator.lastRef/b6m.opacityFactor));
			}
		}

	}
	delete(b6m.node[id].newData.originators);
}

/**
 * Update a link of a node.
 */
function updateLink(id,oldLink,newLink)
{
	var destination = b6m.node[id].data.b6mLinks[oldLink].globalId;
	if(!existsNode(destination)) return;

	if(!b6m.node[id].data.b6mLinks[oldLink].vector)
	{
		return;
	}

	var originNewLonLat = new OpenLayers.LonLat(
		b6m.node[id].data.b6mGeoposition.longitude,
		b6m.node[id].data.b6mGeoposition.latitude
	).transform(
		new OpenLayers.Projection('EPSG:4326'),
		new OpenLayers.Projection('EPSG:900913')
	);
	var originOldLonLat = new OpenLayers.LonLat(
		b6m.node[id].data.b6mLinks[oldLink].vector.geometry.components[0].x,
		b6m.node[id].data.b6mLinks[oldLink].vector.geometry.components[0].y
	);

	var destinationNewLonLat = new OpenLayers.LonLat(
		b6m.node[destination].data.b6mGeoposition.longitude,
		b6m.node[destination].data.b6mGeoposition.latitude
	).transform(
		new OpenLayers.Projection('EPSG:4326'),
		new OpenLayers.Projection('EPSG:900913')
	);
	var destinationOldLonLat = new OpenLayers.LonLat(
		b6m.node[id].data.b6mLinks[oldLink].vector.geometry.components[1].x,
		b6m.node[id].data.b6mLinks[oldLink].vector.geometry.components[1].y
	);

	if
	(
		originNewLonLat.lat
		==
		originOldLonLat.lat
		&&
		originNewLonLat.lon
		==
		originOldLonLat.lon
		&&
		destinationNewLonLat.lat
		==
		destinationOldLonLat.lat
		&&
		destinationNewLonLat.lon
		==
		destinationOldLonLat.lon
		&&
		b6m.node[id].data.b6mLinks[oldLink].quality
		==
		b6m.node[id].newData.b6mLinks[newLink].quality
	)
	{
		b6m.node[id].newData.b6mLinks[newLink] = b6m.node[id].data.b6mLinks[oldLink];
	}
	else
	{
		//TODO: Do not erase link, move the origin and the destination of link and move the link to proper layer.
		eraseLink(id,oldLink);
/*
		var newLayer = chooseLinkLayer(b6m.node[id].newData.b6mLinks[newLink].quality);
		var oldLayer = b6m.node[id].data.b6mLinks[oldLink].vector.layer;
		oldLayer.removeFeatures(b6m.node[id].data.b6mLinks[oldLink].vector);
		newLayer.addFeatures(b6m.node[id].data.b6mLinks[oldLink].vector);
		b6m.node[id].newData.b6mLinks[newLink].vector = b6m.node[id].data.b6mLinks[oldLink].vector;
*/
	}
}

/**
 * Exists node.
 */
function existsNode(id)
{
	if(!b6m.node[id] || b6m.node[id].fail)
	{
		getData(id);
		return false;
	}
	return true;
}

/**
 * Update a node.
 */
function updateNode(id)
{
	if(b6m.node[id].fail) return;

	eraseLinks(id);

	drawNode(id);

	drawLinks(id);
}

/**
 * Draw a node.
 */
function drawNode(id)
{

	if(!b6m.node[id].data.descriptions) return;

	if(!hasGeoposition(id))
	{
		if(!b6m.latitude || !b6m.longitude)
		{
			return
		}
		else
		{
			b6m.node[id].data.b6mGeoposition = new Array();
			b6m.node[id].data.b6mGeoposition.longitude = parseFloat(b6m.longitude) + 0.01 * ( Math.random() - 0.5 );
			b6m.node[id].data.b6mGeoposition.latitude = parseFloat(b6m.latitude) + 0.01 * ( Math.random() - 0.5 );
			b6m.node[id].data.b6mGeoposition.mode = "fake";
		}
	}
	else
	{
		// Center the map.
		if(!b6m.latitude || !b6m.longitude)
		{
			b6m.longitude = b6m.node[id].data.b6mGeoposition.longitude;
			b6m.latitude = b6m.node[id].data.b6mGeoposition.latitude;
			centerMap(b6m.longitude, b6m.latitude);
			$('#b6m > #b6mSplash').fadeToggle();
		}
	}

	var lonLat = new OpenLayers.LonLat(
		b6m.node[id].data.b6mGeoposition.longitude,
		b6m.node[id].data.b6mGeoposition.latitude
	).transform(
		new OpenLayers.Projection('EPSG:4326'),
		new OpenLayers.Projection('EPSG:900913')
	);

	if(b6m.node[id].vector)
	{
		b6m.node[id].vector.move(lonLat);

		if(b6m.node[id].marker)
		{
			var nodesLayer = b6m.map.getLayer('nodes');
			nodesLayer.removeMarker(b6m.node[id].marker);
			delete(b6m.node[id].marker);
		}
	}
	else
	{
		eraseNode(id);

		var nodesLayer;
		
		if(offerInternet(id))
		{
			nodesLayer = b6m.map.getLayer('nodesInternet');
		}
		else
		{
			nodesLayer = b6m.map.getLayer('nodesSearch');
		}
		
		b6m.node[id].vector = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(lonLat.lon,lonLat.lat));
		b6m.node[id].vector.id = id;
		nodesLayer.addFeatures(b6m.node[id].vector);
	}
	if(b6m.node[id].data.b6mGeoposition.mode != "fake")
	{
		drawGraphicNode(id);
	}
}

function offerInternet(id)
{
	if(
		b6m.node[id]
		&&
		b6m.node[id].data
		&&
		b6m.node[id].data.descriptions
	)
	{
		for(var description in b6m.node[id].data.descriptions)
		{
			if(
				b6m.node[id].data.descriptions[description].DESC_ADV
				&&
				b6m.node[id].data.descriptions[description].DESC_ADV.extensions
			)
			{
				for(var extension in b6m.node[id].data.descriptions[description].DESC_ADV.extensions)
				{
					if(b6m.node[id].data.descriptions[description].DESC_ADV.extensions[extension].TUN4IN6_NET_EXTENSION)
					{
						for(var net in b6m.node[id].data.descriptions[description].DESC_ADV.extensions[extension].TUN4IN6_NET_EXTENSION)
						{
							if(b6m.node[id].data.descriptions[description].DESC_ADV.extensions[extension].TUN4IN6_NET_EXTENSION[net].networklen == "0")
							{
								return true;
							}
						}
					}
				}
			}
		}
	}
	return false;
}

/**
 * Create a node database.
 */
function createNodeDataBase(id)
{
	if(!b6m.node[id])
	{
		b6m.node[id] = new Array();
		b6m.node[id].data = new Array();
		b6m.node[id].newData = new Array();
	}
}

/**
 * Erase a node and their links.
 */
function eraseNode(id)
{
	if(!b6m.node[id]) return;

	eraseLinks(id);

	if(b6m.node[id].marker)
	{
		var nodesLayer = b6m.map.getLayer('nodes');
		nodesLayer.removeMarker(b6m.node[id].marker);
		delete(b6m.node[id].marker);
	}

	if(b6m.node[id].vector)
	{
		b6m.node[id].vector.layer.removeFeatures(b6m.node[id].vector);
		delete(b6m.node[id].vector);
	}
}

/**
 * Erase links of a node.
 */
function eraseLinks(id)
{

	for(var link in b6m.node[id].data.b6mLinks)
	{
		eraseLink(id,link);
	}

	for(var orig in b6m.node)
	{
		for(var link in b6m.node[orig].data.b6mLinks)
		{
			if(b6m.node[orig].data.b6mLinks[link].globalId == id)
			{
				eraseLink(orig,link);
			}
		}
	}
}

/**
 * Erase a link.
 */
function eraseLink(orig,link)
{
	if(b6m.node[orig].data.b6mLinks[link].vector)
	{
		var layer = b6m.node[orig].data.b6mLinks[link].vector.layer;
		layer.destroyFeatures(b6m.node[orig].data.b6mLinks[link].vector);
		delete(b6m.node[orig].data.b6mLinks[link].vector);
	}
}

/**
 * Get information of a node.
 */
function getInfoNode(id)
{
	var info = document.createElement('div');
	var title = document.createElement('h3');
	title.appendChild(document.createTextNode(id));
	info.appendChild(title);

	var warning = document.createElement('h4');
	if(b6m.node[id].data.b6mGeoposition.mode == "fake")
	{
		warning.appendChild(document.createTextNode('no geolocation, faked it'));
	}
	info.appendChild(warning);

	for(var link in b6m.node[id].data.b6mLinks)
	{
		var dest = b6m.node[id].data.b6mLinks[link].globalId;
		var b = document.createElement('b');
		b.appendChild(document.createTextNode(dest));
		info.appendChild(b);
		info.appendChild(document.createTextNode(' ('+b6m.node[id].data.b6mLinks[link].viaDev+'): '+b6m.node[id].data.b6mLinks[link].quality));
		var br = document.createElement('br');
		info.appendChild(br);
	}

	return info;
}

/**
 * Draw nodes with a image.
 */
function drawGraphicNode(id)
{
	var image = chooseImage(b6m.node[id].data.type);
	var size = new OpenLayers.Size(image.width, image.height);
	var offset = new OpenLayers.Pixel(image.x, image.y);
	var icon = new OpenLayers.Icon(image.url, size, offset);
	var lonLat = new OpenLayers.LonLat(
		b6m.node[id].data.b6mGeoposition.longitude,
		b6m.node[id].data.b6mGeoposition.latitude
	).transform(
		new OpenLayers.Projection('EPSG:4326'),
		new OpenLayers.Projection('EPSG:900913')
	);

	var layer = b6m.map.getLayer('nodes');

	var html = getInfoNode(id).innerHTML;

	drawMarkerWithPopup(layer, id, lonLat, icon, html);
}

/**
 * Select an icon for a node.
 */
function chooseImage(type)
{
	var url = getB6mURL();
	var image = new Array();
	var typeStr = new String(type);
	switch (typeStr.toLowerCase())
	{
		case 'wifi':
		default:
			image = {
				'url' : url+'img/ispyisail_Wireless_WiFi_symbol_2.png',
				'x' : -16 ,
				'y' : -39,
				'width' : 31,
				'height': 39
			};
			break;
	}
	return(image);
}

/**
 * Draw lines as links.
 */
function drawLine(id, link, x1, y1, x2, y2, layer)
{
	var arr = [];
	arr.push(new OpenLayers.Geometry.Point(x1, y1).transform(new OpenLayers.Projection('EPSG:4326'), new
	     OpenLayers.Projection('EPSG:900913')));
	arr.push(new OpenLayers.Geometry.Point(x2, y2).transform(new OpenLayers.Projection('EPSG:4326'), new
		 OpenLayers.Projection('EPSG:900913')));
	var line = new OpenLayers.Geometry.LineString(arr);

	b6m.node[id].data.b6mLinks[link].vector = new OpenLayers.Feature.Vector(line);
	b6m.node[id].data.b6mLinks[link].vector.id = id+'_'+b6m.node[id].data.b6mLinks[link].globalId+'_'+b6m.node[id].data.b6mLinks[link].viaDev;
	layer.addFeatures(b6m.node[id].data.b6mLinks[link].vector);
}

/**
 * Draw a marker with a popup a node.
 */
function drawMarkerWithPopup(lyr,id,pos,icon,html)
{
	createNodeDataBase(id);
	b6m.node[id].marker = new OpenLayers.Marker(pos,icon);
	b6m.node[id].marker.id = id;
	b6m.node[id].marker.events.register(
		'mousedown',
		b6m.node[id].marker,
		function(e)
		{
			if (b6m.node[id].marker.popup == null)
			{

				popupId = 'popup_' + id.replace(/\./g,'_');
				html = html + '<div id="' + popupId + '"><a class="configure" href="">configure</a><div/>';
				b6m.node[id].marker.popup = new OpenLayers.Popup.FramedCloud(
					id,
					pos,
					null,
					html,
					null,
					true
				);
				b6m.map.addPopup(b6m.node[id].marker.popup);
				$('#' + popupId + ' .configure').click(
					function()
					{
						configureEnviroment(id);
						b6m.map.getLayer('pointer').setVisibility(true);
						return false;
					}
				);
			}
			else
			{
				b6m.node[id].marker.popup.destroy();
				delete(b6m.node[id].marker.popup);
			}
		}
	);
	lyr.addMarker(b6m.node[id].marker);
}
